const defaults = {
  normalize: true
}

module.exports = function nuxtMilligram (options) {
  const opts = Object.assign({}, defaults, options)

  if (opts.normalize) {
    this.options.css.push('normalize.css/normalize.css')
  }

  this.options.css.push('milligram/dist/milligram.css')
}
