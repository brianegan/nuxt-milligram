# nuxt-milligram
[![npm (scoped with tag)](https://img.shields.io/npm/v/nuxt-milligram/latest.svg?style=flat-square)](https://npmjs.com/package/nuxt-milligram)
[![npm](https://img.shields.io/npm/dt/nuxt-milligram.svg?style=flat-square)](https://npmjs.com/package/nuxt-milligram)
> The minimalist CSS framework as a Nuxt module

DEPRECATED: Please used the built-in nuxt functionality for adding CSS libs instead. It's far more flexible! E

Example below.

## Nuxt Example

### Add the dependency

From your terminal, in your Nuxt project directory:

```
yarn add milligram
```

or

```
npm install --save milligram 
```

### Update nuxt.config.js

In the root directory of your Nuxt project, edit the `nuxt.config.js` file like so.

```js
module.exports = {
  modules: [
    // your modules here
  ],

  // Add the following paths to your list of custom css files
  css: [
    'normalize.css/normalize.css',
    'milligram/dist/milligram.css'
  ],
}
```
